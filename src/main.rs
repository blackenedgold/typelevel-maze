use std::marker::PhantomData;

pub struct Cell<Maze, X, Y> {
    m: PhantomData<Maze>,
    x: PhantomData<X>,
    y: PhantomData<Y>,
}

impl<Maze, X, Y> Cell<Maze, X, Y> {
    const fn new() -> Self {
        Self {
            m: PhantomData,
            x: PhantomData,
            y: PhantomData,
        }
    }
}

pub trait Nat {
    fn to_n() -> u32;
}

pub struct Z;
pub struct S<N>(N);

type _0 = Z;
type _1 = S<_0>;
type _2 = S<_1>;

impl Nat for Z {
    fn to_n() -> u32 {
        0
    }
}
impl<N: Nat> Nat for S<N> {
    fn to_n() -> u32 {
        N::to_n() + 1
    }
}

impl<Maze, X, Y> Cell<Maze, X, Y>
where
    X: Nat,
    Y: Nat,
{
    fn to_point() -> (u32, u32) {
        (X::to_n(), Y::to_n())
    }
}

/*

  0   1
+   +---+
|       |  0
+---+   +
|   |   |  1
+---+   +


*/
pub mod maze1 {
    use super::{_0, _1};

    pub struct Maze1;
    pub type Cell<X, Y> = super::Cell<Maze1, X, Y>;

    impl Cell<_1, _1> {
        pub fn up(self) -> Cell<_1, _0> {
            println!("{:?}", <Cell<_1, _0>>::to_point());
            Cell::new()
        }
    }

    impl Cell<_1, _0> {
        pub fn left(self) -> Cell<_0, _0> {
            println!("{:?}", <Cell<_0, _0>>::to_point());
            Cell::new()
        }
    }
    impl Cell<_1, _0> {
        pub fn down(self) -> Cell<_1, _1> {
            println!("{:?}", <Cell<_1, _1>>::to_point());
            Cell::new()
        }
    }

    impl Cell<_0, _0> {
        pub fn down(self) -> Cell<_0, _1> {
            println!("{:?}", <Cell<_0, _1>>::to_point());
            Cell::new()
        }
    }

    pub type Start = Cell<_1, _1>;
    pub type Goal = Cell<_0, _0>;
    pub const START: Start = Cell::new();
}

macro_rules! gen_cell {
    ($ty: ty, . , ($upty: ty, $up:tt), ($rightty: ty,  $right:tt), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {
        gen_cell! {$ty, ($upty, $up), ($rightty, $right), ($downty, $down), ($leftty, $left)}
    };
    ($ty: ty, s , ($upty: ty, $up:tt), ($rightty: ty,  $right:tt), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {
        pub type Start = $ty;
        pub const START: $ty = Cell::new();
        gen_cell! {$ty, ($upty, $up), ($rightty, $right), ($downty, $down), ($leftty, $left)}
    };
    ($ty: ty, g , ($upty: ty, $up:tt), ($rightty: ty,  $right:tt), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {
        pub type Goal = $ty;
        gen_cell! {$ty, ($upty, $up), ($rightty, $right), ($downty, $down), ($leftty, $left)}
    };

    ($ty: ty, # , ($upty: ty, $up:tt), ($rightty: ty,  $right:tt), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {};
    ($ty: ty, ($upty: ty, g), ($rightty: ty,  $right:tt), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {
        gen_cell! {$ty, ($upty, .), ($rightty, $right), ($downty, $down), ($leftty, $left)}
    };
    ($ty: ty, ($upty: ty, $up:tt), ($rightty: ty,  g), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {
        gen_cell! {$ty, ($upty, $up), ($rightty, .), ($downty, $down), ($leftty, $left)}
    };
    ($ty: ty, ($upty: ty, $up:tt), ($rightty: ty,  $right:tt), ($downty:ty, g), ($leftty:ty, $left: tt)) => {
        gen_cell! {$ty, ($upty, $up), ($rightty, .), ($downty, .), ($leftty, $left)}
    };
    ($ty: ty, ($upty: ty, $up:tt), ($rightty: ty,  $right:tt), ($downty:ty, $down:tt), ($leftty:ty, g)) => {
        gen_cell! {$ty, ($upty, $up), ($rightty, .), ($downty, $down), ($leftty, .)}
    };

    ($ty: ty, ($upty: ty, .), ($rightty: ty,  $right:tt), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {
        impl $ty {
            #[allow(dead_code)]
            pub fn up(self) -> $upty {
                println!("{:?}", <$upty>::to_point());
                <$upty>::new()
            }
        }
        gen_cell! {$ty, ($upty, #), ($rightty, $right), ($downty, $down), ($leftty, $left)}
    };

    ($ty: ty, ($upty: ty, $up:tt), ($rightty: ty, .), ($downty:ty, $down:tt), ($leftty:ty, $left: tt)) => {
        impl $ty {
            #[allow(dead_code)]
            pub fn right(self) -> $rightty {
                println!("{:?}", <$rightty>::to_point());
                <$rightty>::new()
            }
        }
        gen_cell! {$ty, ($upty, #), ($rightty, #), ($downty, $down), ($leftty, $left)}
    };

    ($ty: ty, ($upty: ty, $up:tt), ($rightty: ty, $right:tt), ($downty:ty, .), ($leftty:ty, $left: tt)) => {
        impl $ty {
            #[allow(dead_code)]
            pub fn down(self) -> $downty {
                println!("{:?}", <$downty>::to_point());
                <$downty>::new()
            }
        }
        gen_cell! {$ty, ($upty, #), ($rightty, #), ($downty, #), ($leftty, $left)}
    };

    ($ty: ty, ($upty: ty, $up:tt), ($rightty: ty, $right:tt), ($downty:ty, $down:tt), ($leftty:ty, .)) => {
        impl $ty {
            #[allow(dead_code)]
            pub fn left(self) -> $leftty {
                println!("{:?}", <$leftty>::to_point());
                <$leftty>::new()
            }
        }
        gen_cell! {$ty, ($upty, #), ($rightty, #), ($downty, #), ($leftty, #)}
    };

    ($ty: ty, ($upty: ty, $up:tt), ($rightty: ty, $right:tt), ($downty:ty, $down:tt), ($leftty:ty, $left:tt)) => {};
}

macro_rules! gen_maze_row {
    ($x: ty, $y: ty, [$up_left:tt $up:tt $($up_rest:tt)*], [$left:tt $cell:tt $right:tt $($rest:tt)*], [$down_left:tt $down:tt $($down_rest:tt)*]) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, $up), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, $right), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, $down), (Cell<$x, $crate::S<$y>>, $left)}
        gen_maze_row! {$crate::S<$x>, $y, [$up $($up_rest)*], [$cell $right $($rest)*], [$down $($down_rest)*]}
    };
    ($x: ty, $y: ty, [$up_left:tt $up:tt], [$left:tt $cell:tt], [$down_left:tt $down:tt]) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, $up), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, #), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, $down), (Cell<$x, $crate::S<$y>>, $left)}
    };
    ($x: ty, $y: ty, [$up_left:tt $up:tt $($up_rest:tt)*], [$left:tt $cell:tt $right:tt $($rest:tt)*], @bottom) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, $up), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, $right), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, #), (Cell<$x, $crate::S<$y>>, $left)}
        gen_maze_row! {$crate::S<$x>, $y, [$up $($up_rest)*] , [ $cell $right $($rest)* ], @bottom }
    };
    ($x: ty, $y: ty, [$up_left:tt $up:tt], [$left:tt $cell:tt], @bottom) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, $up), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, #), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, #), (Cell<$x, $crate::S<$y>>, $left)}
    };
    ($x: ty, $y: ty, @top, [$left:tt $cell:tt $right:tt $($rest:tt)*], [$down_left:tt $down:tt $($down_rest:tt)*]) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, #), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, $right), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, $down), (Cell<$x, $crate::S<$y>>, $left)}
        gen_maze_row! {$crate::S<$x>, $y, @top, [$cell $right $($rest)*], [$down $($down_rest)*]}
    };
    ($x: ty, $y: ty, @top, [$left:tt $cell:tt], [$down_left:tt $down:tt]) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, #), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, #), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, $down), (Cell<$x, $crate::S<$y>>, $left)}
    };
    ($x: ty, $y: ty, @top, [$left:tt $cell:tt $right:tt $($rest:tt)*], @bottom) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, #), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, $right), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, #), (Cell<$x, $crate::S<$y>>, $left)}
        gen_maze_row! {$crate::S<$x>, $y, @top, [$cell $right $($rest)*], @bottom}
    };
    ($x: ty, $y: ty, @top, [$left:tt $cell:tt], @bottom) => {
        gen_cell! {Cell<$crate::S<$x>, $crate::S<$y>>, $cell, (Cell<$crate::S<$x>, $y>, #), (Cell<$crate::S<$crate::S<$x>>, $crate::S<$y>>, #), (Cell<$crate::S<$x>, $crate::S<$crate::S<$y>>>, #), (Cell<$x, $crate::S<$y>>, $left)}
    };
}

macro_rules! gen_maze {
    ($x: ty, $y: ty,  @top, [$($row:tt)*], [$($down:tt)*] $(,[$($rest:tt)*])*, @bottom) => {
        gen_maze_row!{$x, $y, @top, [$($row)*], [$($down)*]}
        gen_maze!{$x, $crate::S<$y>, [$($row)*], [$($down)*] $(,[$($rest)*])*, @bottom}
    };

    ($x: ty, $y: ty,  [$($up:tt)*], [$($row:tt)*], [$($down:tt)*] $(,[$($rest:tt)*])*, @bottom) => {
        gen_maze_row!{$x, $y, [$($up)*], [$($row)*], [$($down)*]}
        gen_maze!{$x, $crate::S<$y>, [$($row)*], [$($down)*] $(,[$($rest)*])*, @bottom}
    };
    ($x: ty, $y: ty,  [$($up:tt)*], [$($row:tt)*],  @bottom) => {
        gen_maze_row!{$x, $y, [$($up)*], [$($row)*], @bottom}
    };

    ($x: ty, $y: ty,  @top, [$($row:tt)*],  @bottom) => {
        gen_maze_row!{$x, $y, @top, [$($row)*], @bottom}
    };
}

macro_rules! maze {
    ($name: ident, $([$($row:tt)*]),*) => {
        pub struct $name;
        pub type Cell<X, Y> = $crate::Cell<$name, X, Y>;
        gen_maze! {$crate::Z, $crate::Z, @top $(, [# $($row)*])*, @bottom}
    };
}

mod maze2 {
    maze! {
        Maze2,
        [g .],
        [# s]
    }
}

mod maze3 {
    maze! {
        Maze3,
        [. . . . . . . . . # . . g],
        [. # # # . # # # # # . # .],
        [. # . # . . . . . . . # .],
        [. # . # # # # # # # . # .],
        [. . . # . . . . . # . # .],
        [# # # # . # . # . # # # .],
        [. . . . . # . # . . . . .],
        [. # # # # # . # # # # # #],
        [. . . . . # . # . . . . .],
        [. # # # . # . # . # # # .],
        [. # . . . # . # . . . # .],
        [# # . # # # . # # # # # .],
        [s . . # . . . . . . . . .]
    }
}

fn solve_maze1(start: maze1::Start) -> maze1::Goal {
    start.up().left()
}

fn solve_maze2(start: maze2::Start) -> maze2::Goal {
    start.up().left()
}

fn solve_maze3(start: maze3::Start) -> maze3::Goal {
    // solve this!
    unimplemented!()
}

fn main() {
    println!("starting from {:?}", maze3::Start::to_point());
    let _goal = solve_maze3(maze3::START);
    println!("goal!");
}
