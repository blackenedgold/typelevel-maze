Rust type level maze.
When it compiles, it means you solved the maze!

```console
cargo run
```

See also: https://keens.github.io/blog/2019/10/06/rustdekatareberumeiro/
